﻿(function (g) {
    "use strict";
    var mm = {
        /**
         * 機能拡張GetterSetterの生成
         * 
         * バリデーションチェック、値セット前処理、値セット後処理を付与したGetterSetterを生成する。
         * バリデーションチェックの実行は mm.checkFormData() または mm.checkFormItemData() を使用する。
         * チェック結果確認は mm.isValid() を使用する。
         * また、valパラメーターにm.prop()で生成したプロパティ値を渡すことで、それを内包するmm.prop()プロパティを作成することができる。
         * 元のオブジェクトを拡張することなく、バリデーションチェックを付与したい際に有用である。
         * 
         * @param {Object} val - プロパティ初期値
         * @param {Function} validator - バリデーター
         * @param {Function} before - 値セット前アクション
         * @param {Function} after - 値セット後アクション
         * 
         * 
         * 
         * ■使用例
         * 
         * //数値チェックバリデーター
         * //引数にはチェックする値、戻り値はエラーメッセージ(ok時は空文字、error時はエラーメッセージ)
         * var validNumber = function(v){
         *     if(/^\d+$/.test(v))
         *         return "";
         *     else
         *         return "入力形式が不正です。";
         * };
         * 
         * var hogeNum = mm.prop(0, validNumber);
         * 
         * console.log(hogeNum()); //0
         * hogeNum(1234);
         * console.log(hogeNum()); //1234
         * 
         * 
         * 
         * ■使用例2
         * 
         * var mmprop = mm.prop("hoge", 
         *     function validator(v) { return ""; }, 
         *     function before(p,b,v) { console.log("[Before] セット前:",  b, " セット後:", v} return true; }, 
         *     function before(p,b,v) { console.log("[After] セット前:",  b, " セット後:", v}; } 
         * );
         * 
         * console.log(mmprop()); //hoge
         * mmprop("foo"); //[Before] セット前:hoge セット後:foo \r\n [After] セット前:hoge セット後:foo
         * console.log(mmprop()); //foo
         * 
         * 
         * 
         * ■使用例3
         * 
         * var m_prop = m.prop(1234);
         * var mm_prop mm.prop(m_prop);
         * 
         * console.log(m_prop()); //1234
         * console.log(mm_prop()); //1234
         * mm_prop(5678);
         * console.log(mm_prop()); //5678
         * console.log(m_prop()); //5678
         * 
         * 
         */
        prop: function (val, validator, before, after) {

            var m_prop = (typeof val === "function") ? val : m.prop(val);
            var beforeAction = before || function (p, b, v) { return true; };
            var afterAction = after || function (p, b, v) { };

            var mm_prop = function () {
                if (arguments.length) {
                    var v = arguments[0];
                    var b = m_prop();
                    if (beforeAction(mm_prop, b, v)) {
                        m_prop(v);
                        afterAction(mm_prop, b, v);
                    }
                }
                return m_prop();
            }

            mm_prop._mm_ = {
                validator: validator,
                error: false,
                errorMessage: ""
            };

            return mm_prop;
        },

        /**
         * バリデーションチェックの実行
         * 
         * mm.prop()で作成したGetterSetterを対象にバリデーションチェックを行う。
         * 入力フォームにバインドしているGetterSetterの入力チェックを一括して行う目的で使用されることを想定している。
         * 
         * チェック結果は、GetterSetterに付与された拡張プロパティ(_mm_)にセットされる。
         * 個別の結果については、mm.isValid() 経由で取得することを想定している。
         * 
         * @param {Object} 入力フォームデータオブジェクト
         * @retrun {isError: boolean, errorMessages: Array} チェック結果とエラーメッセージ。isErrorは全項目についてチェックOKの場合はtrue、1項目でもチェックNGがある場合はfalseとなる。
         * 
         * 
         * 
         * ■使用例
         * 
         * var vm = function(){
         *     this.id = mm.prop(0, function(v) { ... });
         *     this.title = mm.prop("", function(v) { ... });
         *     this.limite = mm.prop(new Date(), function(v) { ... });
         * };
         * 
         * vm.prototype.addRecord = function(){
         *     if(!mm.checkFormData(this).isError){
         *         ExampleModel.add(this.id(), this.title, this.limite())
         *             .then(function success() { console.log("更新成功"); })
         *     }else{
         *         console.log("入力チェックエラー");
         *     }
         * };
         * 
         *
         */
        checkFormData: function (form) {
            var existErrorProp = false;
            var errorMessages = [];

            for (var propName in form) {
                var prop = form[propName];
                if (!prop || !prop._mm_)
                    continue;                
                var checker = function (p) { 
                    var validator = prop._mm_.validator || function () { return ""; };
                    var errMsg = validator.call(form, p);
                    return {
                        isError: errMsg === "" ? false : true,
                        errMsg: errMsg
                    };
                }
                var checkResult = checker(prop());
                prop._mm_.error = checkResult.isError;
                prop._mm_.errorMessage = checkResult.errMsg;
                if (prop._mm_.error) {
                    errorMessages.push(checkResult.errMsg);
                }

                existErrorProp = existErrorProp || prop._mm_.error;
            }

            return {
                isError: existErrorProp,
                errorMessages: errorMessages
            };
        },

        /**
         * 項目毎に個別のバリデーションチェックを行う
         * 
         * 
         * @param {Object} p プロパティオブジェクト
         * @param {Object} v チェックする値。未指定の場合はプロパティ値をチェックする。
         * @retrun {isError: boolean, errorMessage: string} チェック結果とエラーメッセージ。
         * 
         */
        checkFormItemData: function (p, v) {
            if (typeof p !== "function" || !p._mm_)
                return;
            var validator = p._mm_.validator || function () { return ""; };
            var errMsg = validator.call(p, v || p());
            var isError = errMsg !== "";

            p._mm_.error = isError;
            p._mm_.errorMessage = errMsg;

            return {
                isError: isError,
                errorMessage: errMsg
            };
        },

        /**
         * フォーム項目のバリデーションチェック結果取得
         * 
         * 入力チェック結果に応じたスタイル適用の為にビューで使用されることを想定。
         * 
         * 
         * ■ 使用例
         * 
         * var component = {
         * 	  controller : function(){ ... },
         * 	  view : function (ctrl) {
         * 		return m("div", { className: "todo-regist" }, [
         * 			m("div", { className: "todo-regist-item" }, [
         * 				m("label", { className: "todo-regist-item__title" }, "ID"),
         * 				m("input[type=text]", {
         * 					className: "todo-regist-item__value" + (mm.isValid(ctrl.form.id) ? "" : " ng-value"),
         * 					onchange: m.withAttr("value", ctrl.form.id),
         * 					value: ctrl.form.id()
         * 				}),
         * 			]),
         * 
         * 			m("div", { className: "todo-regist-item" }, [
         * 				m("label", { className: "todo-regist-item__title" }, "タイトル"),
         * 				m("input[type=text]", {
         * 					className: "todo-regist-item__value" + (mm.isValid(ctrl.form.title) ? "" : " ng-value"),
         * 					onchange: m.withAttr("value", ctrl.form.title),
         * 					value: ctrl.form.title()
         * 				}),
         * 			]),
         * 
         * 			m("div", { className: "todo-regist-item" }, [
         * 				m("label", { className: "todo-regist-item__title" }, "期限"),
         * 				m("input[type=text]", {
         * 					className: "todo-regist-item__value" + (mm.isValid(ctrl.form.limite) ? "" : " ng-value"),
         * 					onchange: m.withAttr("value", ctrl.form.limite),
         * 					value: ctrl.form.limite({format: "YYYY/MM/DD"})
         * 				}),
         * 			])
         * 		])
         *   }
         * }
         * 
         */
        isValid: function (prop) {
            var mm = prop._mm_;
            return mm === undefined ? true : mm.error == false;
        },

        /**
         * フォーム項目のバリデーションチェック結果メッセージ取得
         * 
         */
        invalidMessage : function(prop){
            var mm = prop._mm_ || { errorMessage : "" };
            return mm.errorMessage;
        }
    };

    g.mm = mm;

})((this || 0).self || global);